# coding: utf-8
import numpy as np


def cross_entropy_error(y, t):
    return - np.sum(t*np.logy)
